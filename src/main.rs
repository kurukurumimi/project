extern crate nom;
extern crate cse262_project;

use cse262_project::program;

fn main() {
  let result = program("fn main() {
    return foo();
  }
  fn foo(){
    let x = 5;
    return x;
  }");
  println!("{:?}", result);
}
